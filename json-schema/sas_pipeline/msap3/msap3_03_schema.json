{
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "$id": "https://plato_pdc_public.io.ias.u-psud.fr/sas-data-model/sas_pipeline/msap3/msap3_03_schema.json",
    "title": "MSAP3_03 module",
    "description": "03 Global asteroseismic parameters & mode detection",
    "version": "0.0.1",
    "type": "object",
    "properties": {
        "inputs": {
            "title": "Inputs for MSAP3_03 module",
            "type": "object",
            "properties": {
                "IDP_SAS_POWER_EXCESS_PROBABILITY": {
                    "$ref": "../../data_products/idp_sas_power_excess_probability_schema.json#",
                    "description": "POWER EXCESS PROBABILITY: The probability density of Nu max using MSAP3-03 to assess detection flag"
                },
                "IDP_SAS_POWER_EXCESS_METRICS": {
                    "$ref": "../../data_products/idp_sas_power_excess_metrics_schema.json#",
                    "description": "POWER EXCESS PROBABILITY: The metrics and criteria used to compute the probability density of Nu max"
                },
                "IDP_SAS_ACF_PROBABILITY": {
                    "$ref": "../../data_products/idp_sas_acf_probability_schema.json#",
                    "description": "Autocorrelation function probability: Detection probability using ACF of the timeseries"
                },
                "IDP_SAS_ACF_METRICS": {
                    "$ref": "../../data_products/idp_sas_acf_metrics_schema.json#",
                    "description": "Autocorrelation function metrics: Metrics used when determining the detection probability using ACF of the timeseries"
                },
                "IDP_SAS_AARPSD": {
                    "$ref": "../../data_products/idp_sas_aarpsd_schema.json#",
                    "description": "AARPSD[WP12PDP_I32]: Asteroseismic Analysis-ready power spectrum.Frequency-power spectra of the AARLC (AARPS), including information on the spectral window function "
                },
                "IDP_SAS_AARPSD_METADATA": {
                    "$ref": "../../data_products/idp_sas_aarpsd_metadata_schema.json#",
                    "description": "Metadata for AARPSD[WP12PDP_I32]: Metadata for the frequency-power spectra of the AARLC (AARPS), including information on the spectral window function, and the duty cycle"
                },
                "IDP_PFU_TEFF_SAPP": {
                    "$ref": "../../external/mstesci1_schema.json#/properties/IDP_PFU_TEFF_SAPP",
                    "description": "Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200"                
                },
                "IDP_SAS_TEFF_SAPP": {
                    "$ref": "../../data_products/idp_sas_teff_sapp_schema.json#",
                    "description": "Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200."
                },
                "LG_PFU_OBS_DATA": {
                    "$ref": "../../external/mstesci1_schema.json#/properties/LG_PFU_OBS_DATA",
                    "description": "Contains Gaia BP-RP spectrophotometry from final release and Spectra as described in the PDD (under PFU Observed spectrum)"
                }
            },
            "oneOf": [
                {
                    "required": [
                        "IDP_SAS_POWER_EXCESS_PROBABILITY",
                        "IDP_SAS_POWER_EXCESS_METRICS",
                        "IDP_SAS_ACF_PROBABILITY",
                        "IDP_SAS_ACF_METRICS",
                        "IDP_SAS_AARPSD",
                        "IDP_SAS_AARPSD_METADATA",
                        "IDP_PFU_TEFF_SAPP"
                    ]
                },
                {
                    "required": [
                        "IDP_SAS_POWER_EXCESS_PROBABILITY",
                        "IDP_SAS_POWER_EXCESS_METRICS",
                        "IDP_SAS_ACF_PROBABILITY",
                        "IDP_SAS_ACF_METRICS",
                        "IDP_SAS_AARPSD",
                        "IDP_SAS_AARPSD_METADATA",
                        "IDP_SAS_TEFF_SAPP"
                    ]
                }
            ]
        },
        "outputs": {
            "title": "Outputs for MSAP3_03 module",
            "type": "object",
            "properties": {
                "DP3_SAS_DELTA_NU_AV": {
                    "$ref": "../../data_products/dp3_sas_delta_nu_av_schema.json#",
                    "description": "Average large frequency separation: Average large frequency separation from global analysis of Asteroseismic Analysis Ready Power spectrum (AARPS), or from peak bagging wen available (optional)"
                },
                "DP3_SAS_DELTA_NU_AV_METADATA": {
                    "$ref": "../../data_products/dp3_sas_delta_nu_av_metadata_schema.json#",
                    "description": "Metadata on the average large frequency separation: Metadata on the average large frequency separation, including model and range for estimate (optional)"
                },
                "DP3_SAS_NU_MAX": {
                    "$ref": "../../data_products/dp3_sas_nu_max_schema.json#",
                    "description": "Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)"
                },
                "DP3_SAS_NU_MAX_METADATA": {
                    "$ref": "../../data_products/dp3_sas_nu_max_metadata_schema.json#",
                    "description": "Metadata on the frequency of maximum oscillations power: Metadata on frequency of maximum oscillations power, including model for estimate (optional)"
                },
                "IDP_SAS_BACKGROUND_FIT": {
                    "$ref": "../../data_products/idp_sas_background_fit_schema.json#",
                    "description": "Back-ground fit : Background fit (model and parameter)"
                },
                "IDP_SAS_FREQUENCIES_FIRST_GUESS": {
                    "$ref": "../../data_products/idp_sas_frequencies_first_guess_schema.json#",
                    "description": "IDP_SAS_FREQUENCIES_FIRST_GUESS"
                },
                "IDP_SAS_FREQUENCIES_FIRST_GUESS_METADATA": {
                    "$ref": "../../data_products/idp_sas_frequencies_first_guess_metadata_schema.json#",
                    "description": "First-guess frequency list metadata: Metadata on the first-guess frequency list from peak-bagging preparation (optional)"
                },
                "IDP_SAS_OSC_MODE_HEIGHTS_FIRST_GUESS": {
                    "$ref": "../../data_products/idp_sas_osc_mode_heights_first_guess_schema.json#",
                    "description": "First-guess oscillation modes heights: First-guess oscillation modes heights from peak-bagging preparation (optional)"
                },
                "IDP_SAS_OSC_MODE_WIDTHS_FIRST_GUESS": {
                    "$ref": "../../data_products/idp_sas_osc_mode_widths_first_guess_schema.json#",
                    "description": "First-guess oscillation modes widths: First-guess oscillation modes widths from peak-bagging preparation (optional)"
                },
                "IDP_SAS_MULTIDETECTION_METRICS": {
                    "$ref": "../../data_products/idp_sas_multidetection_metrics_schema.json#",
                    "description": "Multiple Detection Flag: Results and metadata from detection stage of module: Multiple Detection Flag (single or multiple spectra, number)"
                },
                "IDP_SAS_DETECTION_METRICS": {
                    "$ref": "../../data_products/idp_sas_detection_metrics_schema.json#",
                    "description": "Detection metrics: Results and metadata from detection stage of module: Detection metrics (SNR, integrated power) (metadata)"
                },
                "IDP_SAS_SEISMIC_DETECTION_FLAG": {
                    "$ref": "../../data_products/idp_sas_seismic_detection_flag_schema.json#",
                    "description": "Detection flag: Results and metadata from detection stage of module: Detection Flag (4 entries: power excess, large separation, a combination of the two, and peak bagging detection flag)"
                }
            },
            "required": [
                "IDP_SAS_BACKGROUND_FIT",
                "IDP_SAS_MULTIDETECTION_METRICS",
                "IDP_SAS_DETECTION_METRICS",
                "IDP_SAS_SEISMIC_DETECTION_FLAG"
            ]
        }
    },
    "required": [
        "inputs",
        "outputs"
    ]
}