# SAS Data model

Data model for SAS :

- pipeline
- HDF5

## Install

```bash
pip install -r requirements.txt
```

## Generate the data model

```bash
datamodel-codegen --http-ignore-tls --url https://plato_pdc_public.io.ias.u-psud.fr/sas-data-model/sas_hdf5/sas_hdf5.json --input-file-type jsonschema --output sas.py --output-model-type pydantic_v2.BaseModel
```

## Generate the documentation as Markdown

```bash
generate-schema-doc --config template_name=md json-schema/sas_pipeline/sas_schema.json doc/md/sas_data_model.md
```

## Generate the documentation as HTML

```bash
generate-schema-doc json-schema/sas_pipeline/sas_schema.json doc/html/
```
